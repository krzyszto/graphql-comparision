from rest_framework import routers

from example_application import views

router = routers.DefaultRouter()
router.register(r'movies', views.MoviesViewSet)

from rest_framework.viewsets import ModelViewSet

from example_application.models import Movie
from example_application.serializers import MovieSerializer


class MoviesViewSet(ModelViewSet):
    serializer_class = MovieSerializer
    queryset = Movie.objects.all()

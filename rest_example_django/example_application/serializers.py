from rest_framework.serializers import ModelSerializer

from example_application.models import Movie


class MovieSerializer(ModelSerializer):
    class Meta:
        model = Movie
        fields = ('id', 'name')

Create Movie 
```
mutation{
  createMovie(name: "Star Wars"){
    movie{
    id
    name}
  }
}
```

Get all movies

```
{
  allMovies{
    edges{
      node{
        id
        name
      }
    }
  }
}
```
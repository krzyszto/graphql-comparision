import os

from flask import Flask
from flask_sqlalchemy import SQLAlchemy

import graphene
from graphene_sqlalchemy import SQLAlchemyObjectType, SQLAlchemyConnectionField

from flask_graphql import GraphQLView

BASE_DIR = os.path.abspath(os.path.dirname(__file__))

app = Flask(__name__)
app.debug = True

app.config['SQLALCHEMY_DATABASE_URI'] = 'sqlite:///' + os.path.join(BASE_DIR, 'db.sqlite3')
app.config['SQLALCHEMY_COMMIT_ON_TEARDOWN'] = True
app.config['SQLALCHEMY_TRACK_MODIFICATIONS'] = True  # Modules

db = SQLAlchemy(app)


class Movie(db.Model):
    __tablename__ = 'Movies'
    uuid = db.Column(db.Integer, primary_key=True)
    name = db.Column(db.String(256), index=True, unique=True)


class MovieObject(SQLAlchemyObjectType):
    class Meta:
        model = Movie
        interfaces = (graphene.relay.Node,)


db.create_all()


class Query(graphene.ObjectType):
    node = graphene.relay.Node.Field()
    all_movies = SQLAlchemyConnectionField(MovieObject)


class CreateMovie(graphene.Mutation):
    class Arguments:
        name = graphene.String(required=True)

    movie = graphene.Field(lambda: MovieObject)

    def mutate(self, info, name):
        movie = Movie(name=name)
        db.session.add(movie)
        db.session.commit()
        return CreateMovie(movie=movie)


class Mutation(graphene.ObjectType):
    create_movie = CreateMovie.Field()


schema = graphene.Schema(query=Query, mutation=Mutation)

app.add_url_rule(
    '/graphql',
    view_func=GraphQLView.as_view(
        'graphql',
        schema=schema,
        graphiql=True  # for having the GraphiQL interface
    )
)

if __name__ == '__main__':
    app.run()

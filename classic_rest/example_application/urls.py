from django.urls import path

from example_application import views

urlpatterns = [
    path('movies/', views.MovieListCreateAPIView.as_view()),
    path('movies/<pk>/', views.MovieRetrieveUpdateDestroyAPIView.as_view()),
]

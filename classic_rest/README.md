# Classic REST approach
This is django application that shows a classic REST approach

To run the project execute following commands
```bash
pip install -r requirements.txt
python manage.py runserver
python manage.py runserver
```

The application would be served on `localhost:8000`

## REST
This application use simple views to mimic REST approach.
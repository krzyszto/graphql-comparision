Custom query, all movies
```
{
    allMovies {
      edges {
        node {
          id
          actors
          title
        }
      }
    }
}
```

Get single movie
```
{
  movie(id: "TW92aWVUeXBlOjE=") {
    id
    title
  }
}
```


Create Movie
```graphql
mutation{
  createMovie(title: "Star Wars"){
    movie{
      title
    }
  }
}
```

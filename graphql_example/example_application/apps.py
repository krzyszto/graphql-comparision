from django.apps import AppConfig


class ExampleApplicationConfig(AppConfig):
    name = 'example_application'

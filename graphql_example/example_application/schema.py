import graphene
from graphene_django import DjangoObjectType
from graphene_django.filter import DjangoFilterConnectionField
from graphql_relay import from_global_id

from example_application.models import Movie


class MovieType(DjangoObjectType):
    class Meta:
        model = Movie
        filter_fields = {
            'name': ['exact', 'icontains', 'istartswith'],
        }
        interfaces = (graphene.relay.Node,)


class CreateMovie(graphene.Mutation):
    class Arguments:
        name = graphene.String(required=True)

    movie = graphene.Field(lambda: MovieType)

    def mutate(self, info, name):
        new_movie = Movie.objects.create(name=name)
        return CreateMovie(movie=new_movie)


class MovieMutation(graphene.ObjectType):
    create_movie = CreateMovie.Field()


class MovieQuery(object):
    movie = graphene.Field(
        MovieType,
        id=graphene.String(),
        name=graphene.String()
    )
    all_movies = DjangoFilterConnectionField(MovieType)

    def resolve_movie(self, info, **kwargs):
        id = kwargs.get('id')
        name = kwargs.get('name')

        if id is not None:
            return Movie.objects.get(pk=from_global_id(id)[-1])

        if name is not None:
            return Movie.objects.get(name=name)

        return None
